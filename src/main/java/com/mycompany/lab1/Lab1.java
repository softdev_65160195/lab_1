/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab1;

/**
 *
 * @author boat5
 */
import java.util.Scanner;
public class Lab1 {
    private static char[][] board = new char[3][3];
    private static char currentPlayer = 'X';
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        boolean gamefin = false;
        boolean conti = true;
        while(conti == true){
            System.out.println("Welcome To XO Game");
            Array();
            gamefin = false;
            while(!gamefin){    
                Board();
                int[] move = getMove();
                int row = move[0];
                int col = move[1];
                if (Movefailed(row, col)) {
                    board[row][col] = currentPlayer;
                    if (checkWin() == true) {
                        Board();
                        System.out.println("Player " + currentPlayer + " wins!");
                        gamefin = true;
                    } else if(checkDraw() == true) {
                        Board();
                        System.out.println("Draw!");
                        gamefin = true;
                    }else {
                        if (currentPlayer == 'X'){
                            currentPlayer = 'O';
                        }else{
                            currentPlayer = 'X';
                        }
                    }
                }else {
                    System.out.println("Move Failed!!");
                }
            }
            boolean Ss = false;
            while(Ss == false){
                System.out.print("Continue? (y/n): ");
                char a = kb.next().charAt(0);
                if(a == 'y' || a == 'Y'){
                    conti = true;
                    Ss = true;
                } else if(a == 'n' || a == 'N'){
                    conti = false;
                    Ss = true;
                }else{
                    System.out.print("Error Try Again ");
                }
            }
        }
    }       
    
    private static void Array() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                board[i][j] = '-';
            }
        }
    }
    
    public static void Board() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(board[i][j] + " ");
            }
            System.out.println();
        }  
    }
    
    public static int[] getMove(){
        Scanner kb = new Scanner(System.in);
        System.out.print("Player " + currentPlayer + ", enter your move(row and column): ");
        int row = kb.nextInt();
        int col = kb.nextInt();
        return new int[]{row, col}; 
    }
    
    public static boolean Movefailed(int row, int col){
        return row >= 0 && row < 3 && col >= 0 && col < 3 && board[row][col] == '-';
        
    }
    
    public static boolean checkWin(){
        for(int i = 0; i < 3; i++){ //Row -->
            if(board[i][0] != '-' && board[i][0] == board[i][1] && board[i][1] == board[i][2]){
                return true;
            }
        }
        for(int j = 0; j < 3; j++){ //Col ^
            if(board[0][j] != '-' && board[0][j] == board[1][j] && board[1][j] == board[2][j]){
                return true;
            }
        }
        
        if(board[0][0] != '-' && board[0][0] == board[1][1] && board[1][1] == board[2][2]){
            return true;
        }
        if(board[0][2] != '-' && board[0][2] == board[1][1] && board[1][1] == board[2][0]){
            return true;
        }
        return false;
    }
    
    public static boolean checkDraw(){
        for(int i = 0; i < 3; i++){
            for (int j = 0; j < 3; j++){
                if(board[i][j] == '-'){
                    return false;
                }
            }
        }
        return true;
    } 
}